package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public interface Carnivorous {
    void eatMeat();
}
