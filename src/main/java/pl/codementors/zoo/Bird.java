package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public abstract class Bird extends Animal {
    private String featherColor;

    public String getFeatherColor() {
        return featherColor;
    }

    public void setFeatherColor(String featherColor) {
        this.featherColor = featherColor;
    }
}
