package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public class Iguana extends Lizard implements Herbivorous {
    public void hiss() {
        getName();
        System.out.println("hiss");
    }
    /*
    public void name(){
        System.out.println("Iga");
    }
    */

    @Override
    public void eatPlant() {
        System.out.println("Eats plant");
    }

    @Override
    public void eat() {

    }
}