package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public class Wolf extends Mammal implements Carnivorous {

    public void Howl() {
        System.out.println("Howls");
    }

    public void eat() {
        System.out.println("zre");
    }
    public void name(){
        System.out.println(getName());
    }

    @Override
    public void eatMeat() {
        System.out.println("Eats meat");
    }
}
