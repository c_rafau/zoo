package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public abstract class Lizard extends Animal{
   private String scaleColor;

    public String getScaleColor() {
        return scaleColor;
    }

    public void setScaleColor(String scaleColor) {
        this.scaleColor = scaleColor;
    }
}
