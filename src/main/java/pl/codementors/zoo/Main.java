package pl.codementors.zoo;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public class Main {
    public static void main(String[] args) throws IllegalAccessException, IOException, InstantiationException, NoSuchMethodException, InvocationTargetException, ClassNotFoundException {
        System.out.println("Hello to the ZOO");

        Wolf wolf = new Wolf();
        // wolf.setName("Burek");
        wolf.name();
        wolf.Howl();

        Iguana iga = new Iguana();

        //iga.name();
        iga.hiss();

        Parrot parrot = new Parrot();
        parrot.name();
        parrot.Screech();

        System.out.println("----");

        Scanner odczyt = new Scanner(System.in); //obiekt do odebrania danych od użytkownika

        System.out.println("Podaj ilosc zwierzat: ");
        int IleZwierzakow = odczyt.nextInt();


        Animal[] zwierzeta = new Animal[IleZwierzakow];

        for (int i = 0; i < IleZwierzakow; i++)

        {
            System.out.println("Podaj rodzaj: ");
            String rodzaj = odczyt.next();
            System.out.println("Podaj imie: ");
            String imie = odczyt.next();
            System.out.println("Podaj kolor: ");
            String kolor = odczyt.next();



            // mozna ustawic imie
            if (rodzaj.equals("wilk")) {
                Animal zwierze = new Wolf();
                zwierze.setName(imie);
                ((Mammal)zwierze).setFurColor(kolor);
                zwierzeta[i] = zwierze;
            } else if(rodzaj.equals("iguana")) {
                Animal zwierze = new Iguana();
                zwierze.setName(imie);
                zwierzeta[i] = zwierze;
                ((Lizard)zwierze).setScaleColor(kolor);
            } else if(rodzaj.equals("papuga")) {
                Animal zwierze = new Parrot();
                zwierze.setName(imie);
                zwierzeta[i] = zwierze;
                ((Bird)zwierze).setFeatherColor(kolor);
            }

        }
        print(zwierzeta);
        feed(zwierzeta);
        saveToFile(zwierzeta, "plik.txt");
        howl(zwierzeta);
        hiss(zwierzeta);
        screech(zwierzeta);
        feedWithMeat(zwierzeta);
        feedWithPlant(zwierzeta);
        readFromFile("plik.txt");
        printColors(zwierzeta);
        saveToBinaryFille(zwierzeta, "plik_binarny.txt");
        readFromBinaryFile("plik_binarny.txt");
    }

    static void print(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            System.out.println(animals[i].getName() + " " + animals[i].getClass().getCanonicalName());
        }
    }
    static void feed(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            animals[i].eat();
        }
    }
    static void saveToFile(Animal[] animals, String file) {
        Path path = Paths.get(file);
        try (BufferedWriter writer = Files.newBufferedWriter(path)) {
            for(int i = 0; i < animals.length; i++) {
                writer.write(animals[i].getName() + " " + animals[i].getClass().toString() + "\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    static void howl(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Wolf) {
                Wolf wolf = (Wolf)animals[i];
                wolf.Howl();
            }
        }
    }
    static void hiss(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Iguana) {
                Iguana iguana = (Iguana) animals[i];
                iguana.hiss();
            }
        }
    }
    static void screech(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Parrot) {
                Parrot parrot = (Parrot) animals[i];
                parrot.Screech();
            }
        }
    }
    static void feedWithMeat(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Carnivorous) {
                ((Carnivorous)animals[i]).eatMeat();
            }
        }
    }
    static void feedWithPlant(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Herbivorous) {
                ((Herbivorous)animals[i]).eatPlant();
            }
        }
    }
    static List<Animal> readFromFile(String file) throws IOException, ClassNotFoundException, IllegalAccessException, InvocationTargetException, InstantiationException, NoSuchMethodException {
        Path path = Paths.get(file);
        List<String> lines = Files.readAllLines(path);
        List<Animal> animals = new ArrayList<>();
        for(String line : lines) {
            String[] splited = line.split("\\s+");
            System.out.println(splited[2]);
            Class<?> clazz = Class.forName(splited[2]);
            Constructor<?> ctor = clazz.getConstructor();
            Object object = ctor.newInstance();
            animals.add((Animal)object);
        }
        return animals;
    }
    static void printColors(Animal[] animals) {
        for(int i = 0; i < animals.length; i++) {
            if(animals[i] instanceof Mammal) {
                System.out.println(((Mammal)animals[i]).getFurColor());
            } else if(animals[i] instanceof Lizard) {
                System.out.println(((Lizard)animals[i]).getScaleColor());
            } else if(animals[i] instanceof Bird) {
                System.out.println(((Bird)animals[i]).getFeatherColor());
            }
        }
    }
    static void saveToBinaryFille(Animal[] animals, String file) throws IOException {
        FileOutputStream out = new FileOutputStream(file);
        ObjectOutputStream oout = new ObjectOutputStream(out);

        for(Animal animal : animals) {
            oout.writeObject(animal);

        }
    }

    static List<Animal> readFromBinaryFile(String file) throws ClassNotFoundException, IOException {
        List<Animal> lista = new ArrayList<>();
        ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file));



        while(true){
            try{
                lista.add((Animal) ois.readObject());
            } catch (EOFException e) {
                return lista;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

}

