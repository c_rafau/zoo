package pl.codementors.zoo;

/**
 * Created by rschoeneck on 05.06.2017.
 */
public abstract class Mammal extends Animal {
    private String furColor;

    public String getFurColor() {
        return furColor;
    }

    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }
}
